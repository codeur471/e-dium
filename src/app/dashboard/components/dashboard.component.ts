import {Component, OnInit} from '@angular/core'
import {TopBarComponent} from "@/shared/components/topBar/topBar.component";
import {BannerComponent} from "@/shared/components/banner/banner.component";
import {FeedComponent} from "@/shared/components/feed/feed.component";
import {FeedTogglerComponent} from "@/shared/components/feedToggler/feedToggler.component";
import {AsideComponent} from "@/shared/components/aside/aside.component";
import {SidebarComponent} from "@/shared/components/sidebar/sidebar.component";
import {FooterComponent} from "@/shared/components/footer/footer.component";
import {PaginationComponent} from "@/shared/components/pagination/pagination.component";
import {Store} from "@ngrx/store";
import {authActions} from "@/auth/store/actions";

@Component({
  selector: 'mc-dashboard',
  templateUrl: './dashboard.component.html',
  standalone: true,
  imports: [
    TopBarComponent,
    BannerComponent,
    FeedComponent,
    FeedTogglerComponent,
    AsideComponent,
    SidebarComponent,
    FooterComponent,
    PaginationComponent
  ]
})
export class DashboardComponent implements OnInit {
  apiUrl = '/articles'

  constructor(private store: Store) {}
  ngOnInit(): void {
    this.store.dispatch(authActions.getCurrentUser())
  }
}

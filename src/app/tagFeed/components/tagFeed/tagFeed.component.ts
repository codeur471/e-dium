import {Component, OnInit} from '@angular/core'
import {ActivatedRoute, Params} from '@angular/router'
import {FeedComponent} from 'src/app/shared/components/feed/feed.component'
import {AsideComponent} from "@/shared/components/aside/aside.component";
import {FooterComponent} from "@/shared/components/footer/footer.component";
import {SidebarComponent} from "@/shared/components/sidebar/sidebar.component";
import {TopBarComponent} from "@/shared/components/topBar/topBar.component";

@Component({
  selector: 'mc-tag-feed',
  templateUrl: './tagFeed.component.html',
  standalone: true,
  imports: [
    FeedComponent,
    AsideComponent,
    FooterComponent,
    SidebarComponent,
    TopBarComponent,
  ],
})
export class TagFeedComponent implements OnInit {
  apiUrl: string = ''
  tagName: string = ''

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.tagName = params['slug']
      this.apiUrl = `/articles?tag=${this.tagName}`
    })
  }
}

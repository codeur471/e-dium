import {Route} from '@angular/router'
import {provideState} from '@ngrx/store'
import {UserProfileService} from './services/userProfile.service'
import {userProfileFeatureKey, userProfileReducer} from './store/reducers'
import * as userProfileEffects from './store/effects'
import {provideEffects} from '@ngrx/effects'
import {ProfileComponent} from "@/profile/component/profile.component";

export const routes: Route[] = [
  {
    path: '',
    component: ProfileComponent,
    providers: [
      UserProfileService,
      provideState(userProfileFeatureKey, userProfileReducer),
      provideEffects(userProfileEffects),
    ],
  },
]

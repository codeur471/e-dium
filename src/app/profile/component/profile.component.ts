import {CommonModule} from '@angular/common'
import {Component, computed, inject, OnInit} from '@angular/core'
import {
  ActivatedRoute,
  Params,
  Router,
  RouterLink,
  RouterLinkActive,
} from '@angular/router'
import {select, Store} from '@ngrx/store'
import {combineLatest, filter, map, of} from 'rxjs'
import {selectCurrentUser} from 'src/app/auth/store/reducers'
import {FeedComponent} from 'src/app/shared/components/feed/feed.component'
import {CurrentUserInterface} from 'src/app/shared/types/currentUser.interface'
import {selectError, selectIsLoading, selectUserProfileData} from "@/profile/store/reducers";
import {UserProfileInterface} from "@/profile/types/userProfile.interface";
import {userProfileActions} from "@/profile/store/actions";
import {TopBarComponent} from "@/shared/components/topBar/topBar.component";
import {AsideComponent} from "@/shared/components/aside/aside.component";
import {LoadingComponent} from "@/shared/components/loading/loading.component";
import {authActions} from "@/auth/store/actions";

@Component({
  selector: 'mc-profile',
  templateUrl: './profile.component.html',
  standalone: true,
  imports: [CommonModule,
    RouterLink,
    RouterLinkActive,
    FeedComponent, TopBarComponent, AsideComponent, LoadingComponent],
})
export class ProfileComponent implements OnInit {
  route = inject(ActivatedRoute)
  store = inject(Store)
  router = inject(Router)
  slug: string = ''
  isCurrentUserProfile$ = combineLatest({
    currentUser: this.store.pipe(
      select(selectCurrentUser),
      filter((currentUser): currentUser is CurrentUserInterface =>
        Boolean(currentUser)
      )
    ),
    userProfile: this.store.pipe(
      select(selectUserProfileData),
      filter((userProfile): userProfile is UserProfileInterface =>
        Boolean(userProfile)
      )
    ),
  }).pipe(
    map(({currentUser, userProfile}) => {
      return currentUser.username === userProfile.username
    })
  )
  data$ = combineLatest({
    isLoading: this.store.select(selectIsLoading),
    error: this.store.select(selectError),
    userProfile: this.store.select(selectUserProfileData),
    isCurrentUserProfile: this.isCurrentUserProfile$,
  })

  isLoading = this.store.selectSignal(selectIsLoading)
  foo = computed(() => (this.isLoading() ? 'true' : 'false'))

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.slug = params['slug']
      this.fetchUserProfile()
    })
    this.store.dispatch(authActions.getCurrentUser())
  }

  fetchUserProfile(): void {
    this.store.dispatch(userProfileActions.getUserProfile({slug: this.slug}))
  }

  getApiUrl(): string {
    const isFavorites = this.router.url.includes('favorites')
    return isFavorites
      ? `/articles?favorited=${this.slug}`
      : `/articles?author=${this.slug}`
  }
}

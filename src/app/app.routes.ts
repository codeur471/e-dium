import {Route} from '@angular/router'
export const appRoutes: Route[] = [
  {
    path: '',
    loadChildren: () =>
      import('src/app/auth/auth.routes').then((m) => m.loginRoutes),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('src/app/auth/auth.routes').then((m) => m.loginRoutes),
  },
  {
    path: 'register',
    loadChildren: () =>
      import('src/app/auth/auth.routes').then((m) => m.registerRoutes),
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('src/app/dashboard/dashboard.routes').then((m) => m.routes),
  },
  {
    path: 'tags/:slug',
    loadChildren: () =>
      import('src/app/tagFeed/tagFeed.routes').then((m) => m.routes),
  },
  {
    path: 'profile/:slug',
    loadChildren: () =>
      import('src/app/profile/profile.routes').then((m) => m.routes),
  },
  {
    path: 'profile/:slug/favorites',
    loadChildren: () =>
      import('src/app/profile/profile.routes').then((m) => m.routes),
  },
]

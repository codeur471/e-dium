export interface ProfileInterface {
  username: string
  bio: string | null
  image: string
  following: boolean
  email: string
}

import {Component, Input} from '@angular/core'
import {CommonModule} from "@angular/common";
import {RouterLink, RouterLinkActive} from "@angular/router";
import {selectCurrentUser} from "../../../auth/store/reducers";
import {Store} from "@ngrx/store";

@Component({
  selector: 'mc-aside',
  templateUrl: './aside.component.html',
  imports: [
    RouterLink, RouterLinkActive, CommonModule
  ],
  standalone: true
})
export class AsideComponent {
  @Input() tagName?: string

  currentUser$ = this.store.select(selectCurrentUser)

  constructor(private store: Store) {}
}

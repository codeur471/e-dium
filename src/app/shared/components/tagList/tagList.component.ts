import {CommonModule} from '@angular/common'
import {Component, Input} from '@angular/core'
import {PopularTagType} from '../../types/popularTag.type'

@Component({
  selector: 'mc-tag-list',
  templateUrl: './tagList.component.html',
  standalone: true,
  imports: [CommonModule],
})
export class TagListComponent {
  @Input() tags: PopularTagType[] = []

  getClassForIndex(index: number): string {
    switch (index) {
      case 0:
        return 'badge badge-light-secondary';
      case 1:
        return 'badge badge-light-danger';
      case 2:
        return 'badge badge-light-primary';
      case 3:
        return 'badge badge-light';
      case 4:
        return 'badge badge-light-success';
      case 5:
        return 'badge badge-light-info';
      case 6:
        return 'badge badge-light-warning';
      case 7:
        return 'badge badge-light-dark';
      default:
        return 'badge badge-light-primary';
    }
  }
}

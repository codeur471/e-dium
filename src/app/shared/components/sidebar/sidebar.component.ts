import {CommonModule} from '@angular/common'
import {Component, OnInit} from '@angular/core'
import {RouterLink} from '@angular/router'
import {Store} from '@ngrx/store'
import {combineLatest} from 'rxjs'
import {ErrorMessageComponent} from '../errorMessage/errorMessage.component'
import {LoadingComponent} from '../loading/loading.component'
import {popularTagsActions} from './store/actions'
import {
  selectError,
  selectIsLoading,
  selectPopularTagsData,
} from './store/reducers'

@Component({
  selector: 'mc-sidebar',
  templateUrl: './sidebar.component.html',
  standalone: true,
  imports: [CommonModule, LoadingComponent, ErrorMessageComponent, RouterLink],
})
export class SidebarComponent implements OnInit {
  data$ = combineLatest({
    popularTags: this.store.select(selectPopularTagsData),
    isLoading: this.store.select(selectIsLoading),
    error: this.store.select(selectError),
  })
  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(popularTagsActions.getPopularTags())
  }

  getClassForIndex(index: number): string {
    switch (index) {
      case 0:
        return 'badge badge-light-secondary';
      case 1:
        return 'badge badge-light-danger';
      case 2:
        return 'badge badge-light-primary';
      case 3:
        return 'badge badge-light';
      case 4:
        return 'badge badge-light-success';
      case 5:
        return 'badge badge-light-info';
      case 6:
        return 'badge badge-light-warning';
      case 7:
        return 'badge badge-light-dark';
      default:
        return 'badge badge-light-primary';
    }
  }
}
